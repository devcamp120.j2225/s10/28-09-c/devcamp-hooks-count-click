import { useEffect, useState } from "react";

function Count() {
    const [count, setCount] = useState(0);

    const increaseCount = () => {
        console.log("increase count");
        setCount(count + 1);
    }

    useEffect(() => {
        console.log("componentDidUpdate!");
        document.title = `You clicked ${count} times!`;
        return () => {
            console.log("componentWillUnmount");
        }
    })

    useEffect(() => {
        console.log("componentDidMount!");
        document.title = `You clicked ${count} times!`;
    }, [])    

    return (
        <>
            <p>You clicked {count} times!</p>
            <button onClick={increaseCount}>Click me!</button>
        </>
    )
}

export default Count;